--[[
  * SigmaSoldi3R's Admin Report System
  * Created on 16 of june, 2014
  * Job for: 'Finnytom'
--]]



--Checks if the Executing Machine is Client or Server

if (SERVER) then
  
  local reports = { }
  
  --Gets the actual time that you need to wait
  local function getCooldown(ply)
    return math.Round(ply.ams_cooldown-CurTime())
  end
  
  local function init()
    
    Msg("==============================\n")
    Msg("- Admin Report System Loaded -\n")
    Msg("By SigmaSoldi3R\n")
    Msg("Have Fun!\n")
    Msg("\nOriginaly Created for Finnytom\n")
    Msg("==============================\n")
    
    --Creates a server convar for the cooldown
    if (!IsValid(GetConVar("ams_cooldown"))) then
      
      --Stores a new ConVar, default 300 Seconds
      CreateConVar("ams_cooldown", 300, {FCVAR_ARCHIEVE, FCVAR_NOTIFY})
      
    end
    
    --Checks whenever if the directory of raports exists
    if (!file.Exists("ARMReports", "DATA")) then
      file.CreateDir("ARMReports")
    end
    
    
  end
  hook.Add("Initialize","AMS_Init",init)
  
  --Spawn Function
  local function spoon(ply)
    
    --Sets the first cooldown timing reference
    ply.ams_cooldown = CurTime()
    
  end
  hook.Add("PlayerSpawn","AMS_Spawn",spoon)
  
  --Networked comminication
  util.AddNetworkString("AdminMessager_SigmaMSG_Open")
  util.AddNetworkString("AdminMessager_SigmaMSG_AdminOpen")
  util.AddNetworkString("AdminMessager_SigmaMSG_SendReport")
  util.AddNetworkString("AdminMessager_SigmaMSG_Message")
  util.AddNetworkString("AdminMessager_SigmaMSG_SolveReport")
  util.AddNetworkString("AdminMessager_SigmaMSG_OpenMenuAdmin")
  util.AddNetworkString("AdminMessager_SigmaMSG_OpenMenuGuest")
  --Open menu function
  local function openMenu(ply, Request)
    
    local RaportList = {}
    RaportList = file.Find("armreports/*", "DATA")
    
    local readReportData = {}
    
    for k,v in pairs(RaportList) do
      
      local bulk = file.Read("armreports/"..v, "DATA")
      
      local divide = string.Explode("\n", bulk)
      
      local maxCount = table.Count(divide)
      
      local rDataw = ""
      
      for i = 5, maxCount, 1 do
        rDataw = rDataw..divide[i].."\n"
      end
      
      local Typewrf = string.Explode("'", divide[4])
      
      local Typew = Typewrf[2]
      
      readReportData[k] = {
        
        author = string.sub(divide[1], 6),
        rdate = string.sub(divide[3], 6),
        rtype = Typew,
        rid = string.sub(divide[2], 10),
        report = rDataw,
        filename = v
        
      }
      
    end
    
    if (Request == "admin") then
      if (ply:IsAdmin()) then
        net.Start("AdminMessager_SigmaMSG_AdminOpen")
          net.WriteTable(readReportData)
        net.Send(ply)
      end
    elseif (Request == "guest") then
      if (ply.ams_cooldown <= CurTime()) then
        net.Start("AdminMessager_SigmaMSG_Open")
        net.Send(ply)
      else
        net.Start("AdminMessager_SigmaMSG_Message")
        net.WriteString("cooldown")
        net.WriteFloat(getCooldown(ply))
        net.Send(ply)
      end
    end
  
  end
  net.Receive("AdminMessager_SigmaMSG_OpenMenuAdmin",function(len, ply)
      openMenu(ply, "admin")
    end)
  net.Receive("AdminMessager_SigmaMSG_OpenMenuGuest",function(len, ply)
      openMenu(ply, "guest")
    end)
  
  function generateReport(ply, msg, date, Type)
    
    --Sets the new cooldown!
    ply.ams_cooldown = CurTime()+GetConVar("ams_cooldown"):GetInt()
    
    local files = file.Find("armreports/*", "DATA")
    
    local c = table.Count(files)
    
    local pathF = "armreports/Report_"..c..".txt"
    
    if (!file.Exists(pathF,"DATA")) then
      file.Write(pathF,"")
    end
    
    --Flush the data to the file system
    local ReportData = "User: "..ply:Name().."\nSteam ID: "..ply:SteamID().."\nDate: "..date.."\nSent a report of '"..Type.."' With following:\n"..msg
    
    file.Append(pathF,ReportData)
    
    --Notifies to console
    Msg("SERVER: Report arrived!\n")
    Msg("SERVER: From player "..ply:Name().." ("..ply:SteamID()..")\n")
    Msg("SERVER:\n----------------------------------------------------\n"..msg)
    Msg("\n----------------------------------------------------\n")
    Msg("SERVER: At "..date.."\n")
    Msg("SERVER: Of type "..Type.."\n")
    
    net.Start("AdminMessager_SigmaMSG_Message")
    net.WriteString("sent")
    net.WriteFloat(0)
    net.Send(ply)
    
    for k,v in pairs(player.GetAll()) do
      if v:IsAdmin() then
        net.Start("AdminMessager_SigmaMSG_Message")
        net.WriteString("received")
        net.WriteFloat(0)
        net.Send(v)
      end
    end
    
  end
  
  net.Receive("AdminMessager_SigmaMSG_SendReport",function(len, ply)
      
      
      local Type = net.ReadString()
      local Date = net.ReadString()
      local Message = net.ReadString()
      
      generateReport(ply, Message, Date, Type)
      
    end)
  
  
  net.Receive("AdminMessager_SigmaMSG_SolveReport",function(len, ply)
      
      
      local roam = tostring("armreports/"..net.ReadString())
      
      file.Delete(roam)
      
      Msg("Ramovin "..roam.."\n")

    end)
  
end


if (CLIENT) then
  
  surface.CreateFont( "ARMFont1", {
	font = "Arial",
	size = 24,
	weight = 1000,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
} )

surface.CreateFont( "ARMFont2", {
	font = "Arial",
	size = 18,
	weight = 1000,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = true,
	additive = false,
	outline = false,
} )

surface.CreateFont( "ARMFont3", {
	font = "Arial",
	size = 28,
	weight = 1000,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
} )

surface.CreateFont( "ARMFont4", {
	font = "Arial",
	size = 18,
	weight = 500,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
} )

surface.CreateFont( "ARMFontGuest", {
	font = "Arial",
	size = 18,
	weight = 1500,
	blursize = 0,
	scanlines = 0,
	antialias = false,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = true,
	additive = false,
	outline = false,
} )
  
  --[[ #### GUEST Message Viewer #### ]]--
  
  local isMenuOpen = false
  local isAdminMenuOpen = false
  
  function openReporter()
    
    local Selection = "Player Report"
    local Contents = "Empty Report!"
    local Date = tostring( os.date() )
    
    local MainFrame = vgui.Create('DFrame')
    MainFrame:SetSize(480, 322)
    MainFrame:Center()
    MainFrame:SetTitle("Admin Report Menu")
    MainFrame:SetBackgroundBlur(true)
    MainFrame:MakePopup()
    MainFrame.Paint = function()
      draw.RoundedBox( 8, 0, 0, MainFrame:GetWide()+12, MainFrame:GetTall()+12, Color( 0, 0, 0, 150 ) )
    end
    MainFrame:ShowCloseButton( false ) 
    
    local CloseButtonOver = vgui.Create('DButton',MainFrame)
    CloseButtonOver:SetText( "X" )
    CloseButtonOver:SetSize(20,16)
    CloseButtonOver:SetPos(460,0)
    CloseButtonOver.DoClick = function()
      isMenuOpen = false
      MainFrame:Close()
    end
    
    local TextBox = vgui.Create('DTextEntry',MainFrame)
    TextBox:SetMultiline(true)
    TextBox:SetSize(474,128)
    TextBox:SetPos(3,139)
    TextBox:SetEnterAllowed()
    
    local ChoicePane = vgui.Create('DPanel', MainFrame)
    ChoicePane:SetSize(466,50)
    ChoicePane:SetPos(8,25)
    ChoicePane.Paint = function()
      draw.RoundedBox( 8, 0, 0, ChoicePane:GetWide(), ChoicePane:GetTall(), Color( 0, 0, 0, 150 ) )
    end
    
    local RLabel1 = vgui.Create('DLabel',MainFrame)
    RLabel1:SetText("Select Report Type: ")
    RLabel1:SetPos(12,35)
    RLabel1:SetSize(500,30)
    RLabel1:SetFont("ARMFontGuest")
    
    local RLabel2 = vgui.Create('DLabel',MainFrame)
    RLabel2:SetText("Write a short description:")
    RLabel2:SetPos(8,110)
    RLabel2:SetSize(500, 30)
    RLabel2:SetFont("ARMFontGuest")
    
    local ReportType = vgui.Create('DComboBox',MainFrame)
    ReportType:SetPos( 310, 35 )
    ReportType:SetSize( 150, 30 )
    ReportType:SetValue( "Player Report" )
    ReportType:AddChoice( "Player Report" )
    ReportType:AddChoice( "Server Status" )
    ReportType:AddChoice( "Bugs" )
    ReportType:AddChoice( "Other/misc" )
    ReportType:SetFont("ARMFontGuest")
    ReportType.OnSelect = function( panel, index, value )
      Selection = value
    end
    
    local Send = vgui.Create('DButton',MainFrame)
    Send:SetSize(474,50)
    Send:SetPos(3,269)
    Send:SetText("SEND REPORT NOW")
    Send:SetFont("ARMFont3")
    Send.DoClick = function()
      Contents = TextBox:GetValue()
      print("Generating Report "..Date)
      print("Sending Report of "..Selection)
      
      net.Start("AdminMessager_SigmaMSG_SendReport")
        net.WriteString( Selection )
        net.WriteString( Date )
        net.WriteString( Contents )
      net.SendToServer()
      
      isMenuOpen = false
      
      MainFrame:Close()
    end
    
    
  end
  
  net.Receive("AdminMessager_SigmaMSG_Open",function(len)
      
      openReporter()
      
    end)
  
  
  
  --[[ #### ADMIN Message Viewer #### ]]--
  local function openAdmin(data)
    
    local MainFrame = vgui.Create('DFrame')
    MainFrame:SetSize(780, 480)
    MainFrame:Center()
    MainFrame:SetTitle("Admin Panel")
    MainFrame:SetBackgroundBlur(true)
    MainFrame:MakePopup()
    MainFrame.Paint = function()
      draw.RoundedBox( 8, 0, 0, MainFrame:GetWide()+6, MainFrame:GetTall()+6, Color( 0, 0, 0, 150 ) )
    end
    MainFrame:ShowCloseButton( false ) 
    
    local CloseButtonOver = vgui.Create('DButton',MainFrame)
    CloseButtonOver:SetText( "X" )
    CloseButtonOver:SetSize(20,16)
    CloseButtonOver:SetPos(760,0)
    CloseButtonOver.DoClick = function()
      isMenuOpen = false
      MainFrame:Close()
    end
    
    local Reports = vgui.Create( 'DListView',  MainFrame)
    Reports:SetPos(10,30)
    Reports:SetSize(338, 441)
    Reports:SetMultiSelect( false )
    Reports:AddColumn( " " ):SetFixedWidth( 16 ) --Icon Column
    Reports:AddColumn( "Type" ):SetFixedWidth( 73 )
    Reports:AddColumn( "Date" ):SetFixedWidth( 100 )
    Reports:AddColumn( "Author" )
    Reports:AddColumn( " " ):SetFixedWidth( 16 )

    
    --[[
    for k,v in pairs(data) do
      Reports:AddLine( v.rtype, v.rdate, v.author )
    end
    ]]--   
    
    for i = 1,(table.Count(data)),1 do
      Image = vgui.Create('DImage', Reports)
      
      if (data[i].rtype == "Bugs") then
        Image:SetImage( "icon16/bug_error.png" )
      elseif (data[i].rtype == "Player Report") then
        Image:SetImage( "icon16/user_delete.png" )
      elseif (data[i].rtype == "Server Status") then
        Image:SetImage( "icon16/computer_error.png" )
      elseif (data[i].rtype == "Other/misc") then
        Image:SetImage( "icon16/information.png" )
      else
        Image:SetImage( "icon16/computer_error.png" )
      end
      
      Image:SizeToContents()
      Reports:AddLine( Image, data[i].rtype, data[i].rdate, data[i].author, i )
    end
    
    local DData = {}
    
    local MetaData = vgui.Create('DPanel', MainFrame)
    MetaData:SetPos(360,30)
    MetaData:SetSize(411,441)
    MetaData:SetVisible(false)
    
    local TypeLabel = vgui.Create('DLabel', MetaData)
    TypeLabel:SetPos(10,10)
    TypeLabel:SetText("type_here")
    TypeLabel:SizeToContents()
    TypeLabel:SetDark(1)
    TypeLabel:SetFont("ARMFont1")
    
    local NameLabel = vgui.Create('DLabel', MetaData)
    NameLabel:SetPos(10,38)
    NameLabel:SetText("name_here")
    NameLabel:SizeToContents()
    NameLabel:SetTextColor(Color(255,220,0,255))
    NameLabel:SetFont("ARMFont2")
    
    local SteamIDLabel = vgui.Create('DLabel', MetaData)
    SteamIDLabel:SetPos(10,60)
    SteamIDLabel:SetText("steam_id_here")
    SteamIDLabel:SizeToContents()
    SteamIDLabel:SetTextColor(Color(50,255,70,255))
    SteamIDLabel:SetFont("ARMFont2")
    
    local ReportDesc = vgui.Create("DTextEntry", MetaData)
    ReportDesc:SetPos(4,90)
    ReportDesc:SetSize(MetaData:GetWide() - 8, MetaData:GetTall() - 138)
    ReportDesc:SetEditable(false)
    ReportDesc:SetDrawBackground(true)
    ReportDesc:SetMultiline(true)
    ReportDesc:SetFont("ARMFont4")
    
    local Selected = 0
    
    local RemoveButton = vgui.Create("DButton", MetaData)
    RemoveButton:SetPos(4, MetaData:GetTall() - 44)
    RemoveButton:SetSize(MetaData:GetWide() - 8, 40)
    RemoveButton:SetText("Solve and Remove")
    RemoveButton:SetTextColor(Color(255,20,20,255))
    RemoveButton:SetFont("ARMFont3")
    RemoveButton.DoClick = function()
      
      local Str = data[Selected].filename
      
      net.Start("AdminMessager_SigmaMSG_SolveReport")
        net.WriteString( Str )
      net.SendToServer()
      
      isAdminMenuOpen = false
      
      MainFrame:Close()
    end
    
    Reports.OnRowSelected = function()
      Selected = Reports:GetSelectedLine()
      DData = data[Reports:GetSelectedLine()]
      Reports:GetSelectedLine()
      MetaData:SetVisible(true)
      NameLabel:SetText("Author Of Report: "..DData.author)
      NameLabel:SizeToContents()
      SteamIDLabel:SetText("Steam ID: "..DData.rid)
      SteamIDLabel:SizeToContents()
      TypeLabel:SetText(DData.rtype)
      TypeLabel:SizeToContents()
      ReportDesc:SetValue(DData.report)
      NameLabel.Paint = function()
        draw.SimpleTextOutlined(NameLabel:GetText(), NameLabel:GetFont(), 0,0,Color(0,0,0,255),0,0,1,Color(0,0,0,255))
      end
      SteamIDLabel.Paint = function()
        draw.SimpleTextOutlined(SteamIDLabel:GetText(), SteamIDLabel:GetFont(), 0,0,Color(0,0,0,255),0,0,1,Color(0,0,0,255))
      end
    end
    
  end
  
  net.Receive("AdminMessager_SigmaMSG_AdminOpen",function(len)
      
      local data = net.ReadTable()
      openAdmin(data)
      
  end)
  
  net.Receive("AdminMessager_SigmaMSG_Message", function(len)
      local kind = net.ReadString()
      local cooldown = net.ReadFloat()
      
    if kind == "sent" then
      chat.AddText(Color(0,255,0,255),"Report sent!")
    elseif kind == "cooldown" then
      chat.AddText(Color(0,200,255),"You need to wait ",Color(255,255,255),tostring(cooldown),Color(0,200,255)," seconds before you make another report.")
    elseif kind == "received" then
      chat.AddText(Color(255,50,10,255),"Report received!")
    end
  end)
  
  
  hook.Add( "Think", "BM_Clients_Key", function()
      
    if ( input.IsKeyDown(KEY_PERIOD) and !isAdminMenuOpen) then
      isAdminMenuOpen = true
      net.Start("AdminMessager_SigmaMSG_OpenMenuAdmin")
      net.SendToServer()
    elseif ( input.IsKeyDown(KEY_COMMA) and !isMenuOpen) then
      isMenuOpen = true
      net.Start("AdminMessager_SigmaMSG_OpenMenuGuest")
      net.SendToServer()
    end
    
  end )
  
end